# cubroid_transfer_learning_with_inception_v3





Transfer Learning 방법을 이용한 이미지 인식 학습 방법입니다.

Transfer Learning이란 이미 학습된 모델을 사용하여 새로운 모델을 만들어내는 방식입니다.
이 방법으로 새로운 모델을 만들시 학습 속도가 빠르고 예측 정확도도 높습니다.

***

### 참고 사이트 
  1. http://cs231n.github.io/transfer-learning/
  2. https://codelabs.developers.google.com/codelabs/tensorflow-for-poets
  3. https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models
  4. https://github.com/googlecodelabs/tensorflow-for-poets-2

***

##### 개발 언어
  - Python 3.6

##### 필요 패키지
  - pip install tensorflow

***

## 사용 예제

## 1. 학습 시키기


이미지 별로 모아서 폴더에 담습니다.
폴더명이 라벨이 됩니다. 직관적인 폴더명을 적어주세요.

![ex_screenshot](./images/folder.png)



"retrain.py" 파이썬 파일은 transfer learning 방식으로 재학습 시키는 소스코드입니다.


        python -m retrain --bottleneck_dir=/tmp/test_1/bottlenecks \
            --model_dir=/tmp/test_1/models/inception_v3 \
            --summaries_dir=/tmp/test_1/training_summaries/inception_v3 \
            --output_graph=/tmp/test_1/retrained_graph.pb \
            --output_labels=/tmp/test_1/retrained_labels.txt \
            --architecture=inception_v3 \
            --image_dir=/tmp/test_1/files



--bottleneck_dir
    = Path to cache bottleneck layer values as files.

--model_dir
    = Path to classify_image_graph_def.pb, imagenet_synset_to_human_label_map.txt, and imagenet_2012_challenge_label_map_proto.pbtxt.

--summaries_dir
    = Where to save summary logs for TensorBoard.

--output_graph
    = 새로 학습이 완료된 그래프의 경로를 지정.

--architecture
    = 이미지 인식 아키텍처 - inception_v3

--image_dir
    = 학습에 사용할 이미지 파일(labeled images) 경로를 지정.



## 2. 학습이 완료된 모델 사용

"label_image.py"


    python -m label_image \
        --graph=/tmp/test_1/retrained_graph.pb \
        --labels=/tmp/test_1/retrained_labels.txt \
        --image=/tmp/test_1/files/img-apple/apple082.jpg \
        --input_height=299 \
        --input_width=299 \
        --input_layer=Mul


--graph
    = graph/model to be executed

--labels
    = name of file containing labels

--image
    = image to be processed

--input_height
    = input height

--input_width
    = input width

--input_layer
    = name of input layer

